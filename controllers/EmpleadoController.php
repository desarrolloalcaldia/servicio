<?php

namespace app\controllers;

use phpDocumentor\Reflection\DocBlock\Tags\Param;
use Yii;
use app\models\Empleado;
use app\models\EmpleadoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmpleadoController implements the CRUD actions for Empleado model.
 */
class EmpleadoController extends Controller
{
    /**
     * @inheritdoc
     */

    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Empleado models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmpleadoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    //carga a un usuario del empleado
    public function actionEmpleados()

    {
        $numdocumento = $_GET['numdocumento'];

        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

        $empleados = Empleado::listarEmpleadosContrato($numdocumento);
        //$empleados= Yii::app()->db->createCommand('tu consulta')->queryAll();


        if (count($empleados) > 0) {

            return array('status' => true, 'data' => $empleados);

        } else {

            return array('status' => false, 'data' => 'No hay empleados');

        }

    }

    public function actionSoap()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

        if (\yii::$app->request->post()) {


            $client = Yii::$app->siteApi;
            $param = array(
                'gestion' => \yii::$app->request->post('gestion'),
                'number' => \yii::$app->request->post('number'),
                'ci' => \yii::$app->request->post('ci')
            );
            $res = $client->GetTramite($param);
            $resultado = $res->GetTramiteResult->OffData;

            $data = array();
            if (is_array($resultado)) {

                for ($i = 0; $i < count($resultado); $i++) {
                    foreach ($resultado[$i] as $key => $value) {
                        $data[$i][$key] = $value;
                    }
                }

            } else {

                foreach ($resultado as $key => $value) {
                    $data[$key] = $value;
                }

            }
            if (count($data) > 0) {

                return array('status' => true, 'data' => $data);

            }
        }

        return array('status' => false, 'data' => 'No hay datos');


    }




    /**
     * Creates a new Empleado model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */


    //Formulario de busqueda de un empleado por un numero de identificacion
    //reutilzo el formulario del create
    public function actionCreate()
    {
        $model = new Empleado();

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['empleados', 'numdocumento' => $model->numdocumento]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays a single Empleado model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing Empleado model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_empleado]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Empleado model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Empleado model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Empleado the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Empleado::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
