<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EmpEmpleado */

$this->title = 'Create Emp Empleado';
$this->params['breadcrumbs'][] = ['label' => 'Emp Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emp-empleado-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
