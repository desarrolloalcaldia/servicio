<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmpEmpleado */

$this->title = 'Update Emp Empleado: ' . $model->id_empleado;
$this->params['breadcrumbs'][] = ['label' => 'Emp Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_empleado, 'url' => ['view', 'id' => $model->id_empleado]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="emp-empleado-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
