<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmpEmpleado */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emp-empleado-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'paterno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'materno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ap_esposo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'numdocumento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nacionalidad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fechanac')->textInput() ?>

    <?= $form->field($model, 'sexo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'celular')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'activo')->textInput() ?>

    <?= $form->field($model, 'expedidoci')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'matricula')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_estadocivil')->textInput() ?>

    <?= $form->field($model, 'cuenta_banco')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'licencia_conducir')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_aporte')->textInput() ?>

    <?= $form->field($model, 'id_ciudad_aportes')->textInput() ?>

    <?= $form->field($model, 'fecha_vencimientoci')->textInput() ?>

    <?= $form->field($model, 'certificadoantecedentes')->textInput() ?>

    <?= $form->field($model, 'fecha_antecedentes')->textInput() ?>

    <?= $form->field($model, 'contrato')->textInput() ?>

    <?= $form->field($model, 'fecha_creacion')->textInput() ?>

    <?= $form->field($model, 'nua')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_lugarseguro')->textInput() ?>

    <?= $form->field($model, 'id_gruposanguineo')->textInput() ?>

    <?= $form->field($model, 'hoja_vida')->textInput() ?>

    <?= $form->field($model, 'seguroaccidentes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'segurovida')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'carnetcps')->textInput() ?>

    <?= $form->field($model, 'id_banco')->textInput() ?>

    <?= $form->field($model, 'id_categoria')->textInput() ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'residencia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ref_ciudad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ref_zona')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ref_calle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ref_nro')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ref_telefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ref_celular')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ref_nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_tipodocumento')->textInput() ?>

    <?= $form->field($model, 'otro_nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'aplica_conduccion')->textInput() ?>

    <?= $form->field($model, 'id_provincia')->textInput() ?>

    <?= $form->field($model, 'id_departamento')->textInput() ?>

    <?= $form->field($model, 'id_seguro')->textInput() ?>

    <?= $form->field($model, 'telefono_coorp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'libreta_militar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_tipovivienda')->textInput() ?>

    <?= $form->field($model, 'con_discapacidad')->textInput() ?>

    <?= $form->field($model, 'familiar_discapacitado')->textInput() ?>

    <?= $form->field($model, 'id_tipoafiliado')->textInput() ?>

    <?= $form->field($model, 'foto')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'pertenencia_ley')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'anios_inst')->textInput() ?>

    <?= $form->field($model, 'meses_inst')->textInput() ?>

    <?= $form->field($model, 'dias_inst')->textInput() ?>

    <?= $form->field($model, 'anios_cons_even_cont')->textInput() ?>

    <?= $form->field($model, 'meses_cons_even_cont')->textInput() ?>

    <?= $form->field($model, 'dias_cons_even_cont')->textInput() ?>

    <?= $form->field($model, 'anios_otras_inst')->textInput() ?>

    <?= $form->field($model, 'meses_otras_inst')->textInput() ?>

    <?= $form->field($model, 'dias_otras_inst')->textInput() ?>

    <?= $form->field($model, 'ubicacion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'id_gradoestudio')->textInput() ?>

    <?= $form->field($model, 'titulo_obtenido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registra_asistencia')->textInput() ?>

    <?= $form->field($model, 'reside_capital')->textInput() ?>

    <?= $form->field($model, 'fechareg_aniosservicio')->textInput() ?>

    <?= $form->field($model, 'antecedentes')->textInput() ?>

    <?= $form->field($model, 'progenitor_gestante')->textInput() ?>

    <?= $form->field($model, 'correo_institucional')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usuario_dominio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'direccion_extranjero')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cobro')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
