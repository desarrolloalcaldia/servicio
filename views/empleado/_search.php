<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmpleadoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empleado-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_empleado') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'paterno') ?>

    <?= $form->field($model, 'materno') ?>

    <?= $form->field($model, 'ap_esposo') ?>

    <?php // echo $form->field($model, 'numdocumento') ?>

    <?php // echo $form->field($model, 'nacionalidad') ?>

    <?php // echo $form->field($model, 'fechanac') ?>

    <?php // echo $form->field($model, 'sexo') ?>

    <?php // echo $form->field($model, 'direccion') ?>

    <?php // echo $form->field($model, 'telefono') ?>

    <?php // echo $form->field($model, 'celular') ?>

    <?php // echo $form->field($model, 'activo') ?>

    <?php // echo $form->field($model, 'expedidoci') ?>

    <?php // echo $form->field($model, 'matricula') ?>

    <?php // echo $form->field($model, 'id_estadocivil') ?>

    <?php // echo $form->field($model, 'cuenta_banco') ?>

    <?php // echo $form->field($model, 'licencia_conducir') ?>

    <?php // echo $form->field($model, 'id_aporte') ?>

    <?php // echo $form->field($model, 'id_ciudad_aportes') ?>

    <?php // echo $form->field($model, 'fecha_vencimientoci') ?>

    <?php // echo $form->field($model, 'certificadoantecedentes') ?>

    <?php // echo $form->field($model, 'fecha_antecedentes') ?>

    <?php // echo $form->field($model, 'contrato') ?>

    <?php // echo $form->field($model, 'fecha_creacion') ?>

    <?php // echo $form->field($model, 'nua') ?>

    <?php // echo $form->field($model, 'id_lugarseguro') ?>

    <?php // echo $form->field($model, 'id_gruposanguineo') ?>

    <?php // echo $form->field($model, 'hoja_vida') ?>

    <?php // echo $form->field($model, 'seguroaccidentes') ?>

    <?php // echo $form->field($model, 'segurovida') ?>

    <?php // echo $form->field($model, 'carnetcps') ?>

    <?php // echo $form->field($model, 'id_banco') ?>

    <?php // echo $form->field($model, 'id_categoria') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'residencia') ?>

    <?php // echo $form->field($model, 'ref_ciudad') ?>

    <?php // echo $form->field($model, 'ref_zona') ?>

    <?php // echo $form->field($model, 'ref_calle') ?>

    <?php // echo $form->field($model, 'ref_nro') ?>

    <?php // echo $form->field($model, 'ref_telefono') ?>

    <?php // echo $form->field($model, 'ref_celular') ?>

    <?php // echo $form->field($model, 'ref_nombre') ?>

    <?php // echo $form->field($model, 'id_tipodocumento') ?>

    <?php // echo $form->field($model, 'otro_nombre') ?>

    <?php // echo $form->field($model, 'aplica_conduccion') ?>

    <?php // echo $form->field($model, 'id_provincia') ?>

    <?php // echo $form->field($model, 'id_departamento') ?>

    <?php // echo $form->field($model, 'id_seguro') ?>

    <?php // echo $form->field($model, 'telefono_coorp') ?>

    <?php // echo $form->field($model, 'libreta_militar') ?>

    <?php // echo $form->field($model, 'id_tipovivienda') ?>

    <?php // echo $form->field($model, 'con_discapacidad') ?>

    <?php // echo $form->field($model, 'familiar_discapacitado') ?>

    <?php // echo $form->field($model, 'id_tipoafiliado') ?>

    <?php // echo $form->field($model, 'foto') ?>

    <?php // echo $form->field($model, 'pertenencia_ley') ?>

    <?php // echo $form->field($model, 'anios_inst') ?>

    <?php // echo $form->field($model, 'meses_inst') ?>

    <?php // echo $form->field($model, 'dias_inst') ?>

    <?php // echo $form->field($model, 'anios_cons_even_cont') ?>

    <?php // echo $form->field($model, 'meses_cons_even_cont') ?>

    <?php // echo $form->field($model, 'dias_cons_even_cont') ?>

    <?php // echo $form->field($model, 'anios_otras_inst') ?>

    <?php // echo $form->field($model, 'meses_otras_inst') ?>

    <?php // echo $form->field($model, 'dias_otras_inst') ?>

    <?php // echo $form->field($model, 'ubicacion') ?>

    <?php // echo $form->field($model, 'id_gradoestudio') ?>

    <?php // echo $form->field($model, 'titulo_obtenido') ?>

    <?php // echo $form->field($model, 'registra_asistencia') ?>

    <?php // echo $form->field($model, 'reside_capital') ?>

    <?php // echo $form->field($model, 'fechareg_aniosservicio') ?>

    <?php // echo $form->field($model, 'antecedentes') ?>

    <?php // echo $form->field($model, 'progenitor_gestante') ?>

    <?php // echo $form->field($model, 'correo_institucional') ?>

    <?php // echo $form->field($model, 'usuario_dominio') ?>

    <?php // echo $form->field($model, 'direccion_extranjero') ?>

    <?php // echo $form->field($model, 'cobro')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
