<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Empleado */

$this->title = 'Buscar Empleado';
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Buscar';
?>
<div class="empleado-create">

    <h1>Buscar empleado</h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
