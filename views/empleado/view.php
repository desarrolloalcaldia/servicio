<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Empleado */

$this->title = $model->id_empleado;
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleado-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_empleado], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_empleado], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_empleado',
            'nombre',
            'paterno',
            'materno',
            'ap_esposo',
            'numdocumento',
            'nacionalidad',
            'fechanac',
            'sexo',
            'direccion',
            'telefono',
            'celular',
            'activo',
            'expedidoci',
            'matricula',
            'id_estadocivil',
            'cuenta_banco',
            'licencia_conducir',
            'id_aporte',
            'id_ciudad_aportes',
            'fecha_vencimientoci',
            'certificadoantecedentes',
            'fecha_antecedentes',
            'contrato',
            'fecha_creacion',
            'nua',
            'id_lugarseguro',
            'id_gruposanguineo',
            'hoja_vida',
            'seguroaccidentes',
            'segurovida',
            'carnetcps',
            'id_banco',
            'id_categoria',
            'email:email',
            'residencia',
            'ref_ciudad',
            'ref_zona',
            'ref_calle',
            'ref_nro',
            'ref_telefono',
            'ref_celular',
            'ref_nombre',
            'id_tipodocumento',
            'otro_nombre',
            'aplica_conduccion',
            'id_provincia',
            'id_departamento',
            'id_seguro',
            'telefono_coorp',
            'libreta_militar',
            'id_tipovivienda',
            'con_discapacidad',
            'familiar_discapacitado',
            'id_tipoafiliado',
            'foto:ntext',
            'pertenencia_ley',
            'anios_inst',
            'meses_inst',
            'dias_inst',
            'anios_cons_even_cont',
            'meses_cons_even_cont',
            'dias_cons_even_cont',
            'anios_otras_inst',
            'meses_otras_inst',
            'dias_otras_inst',
            'ubicacion:ntext',
            'id_gradoestudio',
            'titulo_obtenido',
            'registra_asistencia',
            'reside_capital',
            'fechareg_aniosservicio',
            'antecedentes',
            'progenitor_gestante',
            'correo_institucional',
            'usuario_dominio',
            'direccion_extranjero',
            'cobro:boolean',
        ],
    ]) ?>

</div>
