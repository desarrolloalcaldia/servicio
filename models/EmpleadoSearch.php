<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Empleado;

/**
 * EmpleadoSearch represents the model behind the search form about `app\models\Empleado`.
 */
class EmpleadoSearch extends Empleado
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_empleado', 'activo', 'id_estadocivil', 'id_aporte', 'id_ciudad_aportes', 'certificadoantecedentes', 'contrato', 'id_lugarseguro', 'id_gruposanguineo', 'hoja_vida', 'carnetcps', 'id_banco', 'id_categoria', 'id_tipodocumento', 'aplica_conduccion', 'id_provincia', 'id_departamento', 'id_seguro', 'id_tipovivienda', 'con_discapacidad', 'familiar_discapacitado', 'id_tipoafiliado', 'anios_inst', 'meses_inst', 'dias_inst', 'anios_cons_even_cont', 'meses_cons_even_cont', 'dias_cons_even_cont', 'anios_otras_inst', 'meses_otras_inst', 'dias_otras_inst', 'id_gradoestudio', 'registra_asistencia', 'reside_capital', 'antecedentes', 'progenitor_gestante'], 'integer'],
            [['nombre', 'paterno', 'materno', 'ap_esposo', 'numdocumento', 'nacionalidad', 'fechanac', 'sexo', 'direccion', 'telefono', 'celular', 'expedidoci', 'matricula', 'cuenta_banco', 'licencia_conducir', 'fecha_vencimientoci', 'fecha_antecedentes', 'fecha_creacion', 'nua', 'seguroaccidentes', 'segurovida', 'email', 'residencia', 'ref_ciudad', 'ref_zona', 'ref_calle', 'ref_nro', 'ref_telefono', 'ref_celular', 'ref_nombre', 'otro_nombre', 'telefono_coorp', 'libreta_militar', 'foto', 'pertenencia_ley', 'ubicacion', 'titulo_obtenido', 'fechareg_aniosservicio', 'correo_institucional', 'usuario_dominio', 'direccion_extranjero'], 'safe'],
            [['cobro'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Empleado::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_empleado' => $this->id_empleado,
            'fechanac' => $this->fechanac,
            'activo' => $this->activo,
            'id_estadocivil' => $this->id_estadocivil,
            'id_aporte' => $this->id_aporte,
            'id_ciudad_aportes' => $this->id_ciudad_aportes,
            'fecha_vencimientoci' => $this->fecha_vencimientoci,
            'certificadoantecedentes' => $this->certificadoantecedentes,
            'fecha_antecedentes' => $this->fecha_antecedentes,
            'contrato' => $this->contrato,
            'fecha_creacion' => $this->fecha_creacion,
            'id_lugarseguro' => $this->id_lugarseguro,
            'id_gruposanguineo' => $this->id_gruposanguineo,
            'hoja_vida' => $this->hoja_vida,
            'carnetcps' => $this->carnetcps,
            'id_banco' => $this->id_banco,
            'id_categoria' => $this->id_categoria,
            'id_tipodocumento' => $this->id_tipodocumento,
            'aplica_conduccion' => $this->aplica_conduccion,
            'id_provincia' => $this->id_provincia,
            'id_departamento' => $this->id_departamento,
            'id_seguro' => $this->id_seguro,
            'id_tipovivienda' => $this->id_tipovivienda,
            'con_discapacidad' => $this->con_discapacidad,
            'familiar_discapacitado' => $this->familiar_discapacitado,
            'id_tipoafiliado' => $this->id_tipoafiliado,
            'anios_inst' => $this->anios_inst,
            'meses_inst' => $this->meses_inst,
            'dias_inst' => $this->dias_inst,
            'anios_cons_even_cont' => $this->anios_cons_even_cont,
            'meses_cons_even_cont' => $this->meses_cons_even_cont,
            'dias_cons_even_cont' => $this->dias_cons_even_cont,
            'anios_otras_inst' => $this->anios_otras_inst,
            'meses_otras_inst' => $this->meses_otras_inst,
            'dias_otras_inst' => $this->dias_otras_inst,
            'id_gradoestudio' => $this->id_gradoestudio,
            'registra_asistencia' => $this->registra_asistencia,
            'reside_capital' => $this->reside_capital,
            'fechareg_aniosservicio' => $this->fechareg_aniosservicio,
            'antecedentes' => $this->antecedentes,
            'progenitor_gestante' => $this->progenitor_gestante,
            'cobro' => $this->cobro,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'paterno', $this->paterno])
            ->andFilterWhere(['like', 'materno', $this->materno])
            ->andFilterWhere(['like', 'ap_esposo', $this->ap_esposo])
            ->andFilterWhere(['like', 'numdocumento', $this->numdocumento])
            ->andFilterWhere(['like', 'nacionalidad', $this->nacionalidad])
            ->andFilterWhere(['like', 'sexo', $this->sexo])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'celular', $this->celular])
            ->andFilterWhere(['like', 'expedidoci', $this->expedidoci])
            ->andFilterWhere(['like', 'matricula', $this->matricula])
            ->andFilterWhere(['like', 'cuenta_banco', $this->cuenta_banco])
            ->andFilterWhere(['like', 'licencia_conducir', $this->licencia_conducir])
            ->andFilterWhere(['like', 'nua', $this->nua])
            ->andFilterWhere(['like', 'seguroaccidentes', $this->seguroaccidentes])
            ->andFilterWhere(['like', 'segurovida', $this->segurovida])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'residencia', $this->residencia])
            ->andFilterWhere(['like', 'ref_ciudad', $this->ref_ciudad])
            ->andFilterWhere(['like', 'ref_zona', $this->ref_zona])
            ->andFilterWhere(['like', 'ref_calle', $this->ref_calle])
            ->andFilterWhere(['like', 'ref_nro', $this->ref_nro])
            ->andFilterWhere(['like', 'ref_telefono', $this->ref_telefono])
            ->andFilterWhere(['like', 'ref_celular', $this->ref_celular])
            ->andFilterWhere(['like', 'ref_nombre', $this->ref_nombre])
            ->andFilterWhere(['like', 'otro_nombre', $this->otro_nombre])
            ->andFilterWhere(['like', 'telefono_coorp', $this->telefono_coorp])
            ->andFilterWhere(['like', 'libreta_militar', $this->libreta_militar])
            ->andFilterWhere(['like', 'foto', $this->foto])
            ->andFilterWhere(['like', 'pertenencia_ley', $this->pertenencia_ley])
            ->andFilterWhere(['like', 'ubicacion', $this->ubicacion])
            ->andFilterWhere(['like', 'titulo_obtenido', $this->titulo_obtenido])
            ->andFilterWhere(['like', 'correo_institucional', $this->correo_institucional])
            ->andFilterWhere(['like', 'usuario_dominio', $this->usuario_dominio])
            ->andFilterWhere(['like', 'direccion_extranjero', $this->direccion_extranjero]);

        return $dataProvider;
    }
}
