<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emp_gradoestudio".
 *
 * @property integer $id_gradoestudio
 * @property string $gradoestudio
 * @property integer $activo
 *
 * @property EmpEmpleado[] $empEmpleados
 */
class EmpGradoestudio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emp_gradoestudio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gradoestudio', 'activo'], 'required'],
            [['activo'], 'integer'],
            [['gradoestudio'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_gradoestudio' => 'Id Gradoestudio',
            'gradoestudio' => 'Gradoestudio',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpEmpleados()
    {
        return $this->hasMany(EmpEmpleado::className(), ['id_gradoestudio' => 'id_gradoestudio']);
    }
}
