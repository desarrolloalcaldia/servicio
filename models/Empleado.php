<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emp_empleado".
 *
 * @property integer $id_empleado
 * @property string $nombre
 * @property string $paterno
 * @property string $materno
 * @property string $ap_esposo
 * @property string $numdocumento
 * @property string $nacionalidad
 * @property string $fechanac
 * @property string $sexo
 * @property string $direccion
 * @property string $telefono
 * @property string $celular
 * @property integer $activo
 * @property string $expedidoci
 * @property string $matricula
 * @property integer $id_estadocivil
 * @property string $cuenta_banco
 * @property string $licencia_conducir
 * @property integer $id_aporte
 * @property integer $id_ciudad_aportes
 * @property string $fecha_vencimientoci
 * @property integer $certificadoantecedentes
 * @property string $fecha_antecedentes
 * @property integer $contrato
 * @property string $fecha_creacion
 * @property string $nua
 * @property integer $id_lugarseguro
 * @property integer $id_gruposanguineo
 * @property integer $hoja_vida
 * @property string $seguroaccidentes
 * @property string $segurovida
 * @property integer $carnetcps
 * @property integer $id_banco
 * @property integer $id_categoria
 * @property string $email
 * @property string $residencia
 * @property string $ref_ciudad
 * @property string $ref_zona
 * @property string $ref_calle
 * @property string $ref_nro
 * @property string $ref_telefono
 * @property string $ref_celular
 * @property string $ref_nombre
 * @property integer $id_tipodocumento
 * @property string $otro_nombre
 * @property integer $aplica_conduccion
 * @property integer $id_provincia
 * @property integer $id_departamento
 * @property integer $id_seguro
 * @property string $telefono_coorp
 * @property string $libreta_militar
 * @property integer $id_tipovivienda
 * @property integer $con_discapacidad
 * @property integer $familiar_discapacitado
 * @property integer $id_tipoafiliado
 * @property string $foto
 * @property string $pertenencia_ley
 * @property integer $anios_inst
 * @property integer $meses_inst
 * @property integer $dias_inst
 * @property integer $anios_cons_even_cont
 * @property integer $meses_cons_even_cont
 * @property integer $dias_cons_even_cont
 * @property integer $anios_otras_inst
 * @property integer $meses_otras_inst
 * @property integer $dias_otras_inst
 * @property string $ubicacion
 * @property integer $id_gradoestudio
 * @property string $titulo_obtenido
 * @property integer $registra_asistencia
 * @property integer $reside_capital
 * @property string $fechareg_aniosservicio
 * @property integer $antecedentes
 * @property integer $progenitor_gestante
 * @property string $correo_institucional
 * @property string $usuario_dominio
 * @property string $direccion_extranjero
 * @property boolean $cobro
 *
 * @property AsisDuodecima[] $asisDuodecimas
 * @property AsisVacacion[] $asisVacacions
 * @property AsisVacacionempleado[] $asisVacacionempleados
 * @property DeclaracionJurada[] $declaracionJuradas
 * @property EmpAniosservicio[] $empAniosservicios
 * @property EmpContrato[] $empContratos
 * @property EmpDeclaracionjurada[] $empDeclaracionjuradas
 * @property EmpGradoestudio $idGradoestudio
 * @property EmpPais $nacionalidad0
 * @property EmpTipodocumento $idTipodocumento
 * @property EmpTiposangre $idGruposanguineo
 * @property GralEstadocivil $idEstadocivil
 * @property EmpKardex[] $empKardexes
 * @property EmpLlamadaatencion[] $empLlamadaatencions
 * @property EmpMemoEmpleado[] $empMemoEmpleados
 * @property EmpPrerrequisitoempleado[] $empPrerrequisitoempleados
 * @property EmpProceso[] $empProcesos
 * @property UsrUsuario[] $usrUsuarios
 */
class Empleado extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emp_empleado';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fechanac', 'fecha_vencimientoci', 'fecha_antecedentes', 'fecha_creacion', 'fechareg_aniosservicio'], 'safe'],
            [['activo', 'id_estadocivil', 'id_aporte', 'id_ciudad_aportes', 'certificadoantecedentes', 'contrato', 'id_lugarseguro', 'id_gruposanguineo', 'hoja_vida', 'carnetcps', 'id_banco', 'id_categoria', 'id_tipodocumento', 'aplica_conduccion', 'id_provincia', 'id_departamento', 'id_seguro', 'id_tipovivienda', 'con_discapacidad', 'familiar_discapacitado', 'id_tipoafiliado', 'anios_inst', 'meses_inst', 'dias_inst', 'anios_cons_even_cont', 'meses_cons_even_cont', 'dias_cons_even_cont', 'anios_otras_inst', 'meses_otras_inst', 'dias_otras_inst', 'id_gradoestudio', 'registra_asistencia', 'reside_capital', 'antecedentes', 'progenitor_gestante'], 'integer'],
            [['fecha_creacion'], 'required'],
            [['foto', 'ubicacion'], 'string'],
            [['cobro'], 'boolean'],
            [['nombre', 'ap_esposo', 'nacionalidad', 'licencia_conducir', 'nua', 'email', 'ref_ciudad', 'ref_nro'], 'string', 'max' => 50],
            [['paterno', 'materno'], 'string', 'max' => 30],
            [['numdocumento'], 'string', 'max' => 25],
            [['sexo', 'expedidoci', 'seguroaccidentes', 'segurovida'], 'string', 'max' => 10],
            [['direccion', 'cuenta_banco'], 'string', 'max' => 150],
            [['telefono', 'celular', 'telefono_coorp', 'libreta_militar'], 'string', 'max' => 40],
            [['matricula'], 'string', 'max' => 20],
            [['residencia', 'ref_telefono', 'ref_celular', 'otro_nombre', 'titulo_obtenido'], 'string', 'max' => 100],
            [['ref_zona', 'ref_calle', 'ref_nombre'], 'string', 'max' => 200],
            [['pertenencia_ley', 'usuario_dominio'], 'string', 'max' => 32],
            [['correo_institucional'], 'string', 'max' => 128],
            [['direccion_extranjero'], 'string', 'max' => 256],
            [['id_gradoestudio'], 'exist', 'skipOnError' => true, 'targetClass' => EmpGradoestudio::className(), 'targetAttribute' => ['id_gradoestudio' => 'id_gradoestudio']],
            // [['nacionalidad'], 'exist', 'skipOnError' => true, 'targetClass' => EmpPais::className(), 'targetAttribute' => ['nacionalidad' => 'nacionalidad']],
            // [['id_tipodocumento'], 'exist', 'skipOnError' => true, 'targetClass' => EmpTipodocumento::className(), 'targetAttribute' => ['id_tipodocumento' => 'id_tipodocumento']],
            //[['id_gruposanguineo'], 'exist', 'skipOnError' => true, 'targetClass' => EmpTiposangre::className(), 'targetAttribute' => ['id_gruposanguineo' => 'id_tiposangre']],
            //[['id_estadocivil'], 'exist', 'skipOnError' => true, 'targetClass' => GralEstadocivil::className(), 'targetAttribute' => ['id_estadocivil' => 'id_estadocivil']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_empleado' => 'Clave Primaria',
            'nombre' => 'Nombre del empleado',
            'paterno' => 'Apellido Paterno del Empleado',
            'materno' => 'Apellido Materno del empleado',
            'ap_esposo' => 'Apellido de Casado(a) del empleado',
            'numdocumento' => 'Número de documento de Identificación del empleado',
            'nacionalidad' => 'Clave Foranea de la tabla emp_pais, que identifica la nacionalidad del empleado',
            'fechanac' => 'Fecha de Nacimiento del Empleado',
            'sexo' => 'Genero al que pertenece al empleado',
            'direccion' => 'Dirección del domicilio del empleado',
            'telefono' => 'Número del Telefono Fijo del Empleado',
            'celular' => 'Número de Celular del Empleado',
            'activo' => 'Estado del Empleado',
            'expedidoci' => 'Lugar donde fue expedido el documento de identificación del empleado',
            'matricula' => 'Código de afiliación en el Seguro de Salud',
            'id_estadocivil' => 'Clave Foranea de la tabla emp_empleado que identifica al estado civil del empleado',
            'cuenta_banco' => 'Número de Cuenta de Banco',
            'licencia_conducir' => 'Número  de Licencia de Conducir',
            'id_aporte' => 'Clave Foranea de la tabla emp_aporte, que identifica la entidad en la que se realiza los aportes del empleado',
            'id_ciudad_aportes' => 'Clave Foranea de la tabla emp_cuidadaporte, que identifica la cuidad donde se realizan los aportes del empleado',
            'fecha_vencimientoci' => 'Fecha de vencimiento del documento de identificación del empleado',
            'certificadoantecedentes' => 'Identifica si se presento o no el certificado de antecedentes',
            'fecha_antecedentes' => 'Fecha de presentación del certificado de antecedentes ',
            'contrato' => 'Indentifica si el contrato es de trbajo fisico o  no',
            'fecha_creacion' => 'Fecha y hora de creación del registro del empleado',
            'nua' => 'Código de afiliación a la Afp
',
            'id_lugarseguro' => 'Clave Foranea de la tabla emp_lugarseguro, que identifica el lugar donde se encuentra el seguro de salud',
            'id_gruposanguineo' => 'Clave Foranea de la tabla emp_tiposangre, que identifica el grupo sanguineo al cual pertenece el empleado',
            'hoja_vida' => 'Permite identificar si se presento o no curriculum',
            'seguroaccidentes' => 'Código de Seguro de Accidentes',
            'segurovida' => 'Código de seguro de vida',
            'carnetcps' => 'Permite identificar si el empleado tiene o no carnet de seguro',
            'id_banco' => 'Clave Foranea de la tabla emp_banco, que permite identificar a que banco pertenece el número de cuenta de banco del empleado',
            'id_categoria' => 'Clave Foranea de la tabla emp_categoria, que identifica la categoria del empleado',
            'email' => 'Dirección de correo electronico del empleado',
            'residencia' => 'Lugar de Residencia del empleado',
            'ref_ciudad' => 'Cuidad donde reside la persona que es referencia del empleado',
            'ref_zona' => 'Zona de la cuidad donde reside la persona que es referencia del empleado',
            'ref_calle' => 'Calle donde reside la persona que es referencia del empleado',
            'ref_nro' => 'Nro de casa de la persona que es referencia del empleado',
            'ref_telefono' => 'Nro de Telefono fijo de la persona que es referencia del empleado',
            'ref_celular' => 'Número de telefono celularde la persona que es referencia del empleado',
            'ref_nombre' => 'Nombre Completo de la persona que es referencia del empleado',
            'id_tipodocumento' => 'Clave Foranea de la tabla emp_tipodocumento, que identifica al tipo de documento de identificación del empleado',
            'otro_nombre' => 'Segundo Nombre u otro nombre del Empleado',
            'aplica_conduccion' => 'Permite verificar si el empleado aplica a no a lista de conductores aprobados',
            'id_provincia' => 'Clave Foraneade la tabla gral_provincia, que identifica la provincia a la cual pertenece el empleado',
            'id_departamento' => 'Clave Foraneade la tabla gral_departamento, que identifica el departamento al cual pertenece el empleado',
            'id_seguro' => 'Clave Foraneade la tabla emp_seguro, que identifica al seguro al cual pertenece el empleado',
            'telefono_coorp' => 'Número de telefono coorporativo',
            'libreta_militar' => 'Código de Libreta militar',
            'id_tipovivienda' => 'Clave Foranea de la tabla emp_tipovivienda, que identifica el tipo de vivienda del empleado',
            'con_discapacidad' => 'Permite identificar si el empleado tiene o no alguna discapacidad',
            'familiar_discapacitado' => 'Permite identificar si el empleado tiene o no algun familiar discapacitado',
            'id_tipoafiliado' => 'Clave Foranea de la tabla emp_tipoafiliado, que identifica el tipo de afiliación que tiene el empleado',
            'foto' => 'Códificación en base 64 de la Foto del empleado',
            'pertenencia_ley' => 'Indica el tipo de ley al cual pertenence el empleado',
            'anios_inst' => 'Número de años que trabaja el empleado en la institución',
            'meses_inst' => 'Número de meses que el empleado trabaja en la institucion',
            'dias_inst' => 'Número de dias que el empleado trabaja en la institucion',
            'anios_cons_even_cont' => 'Número de años que el empleado trabajó eventualmente, en consejo y con contrato',
            'meses_cons_even_cont' => 'Número de meses que el empleado trabajó eventualmente, en consejo y con contrato',
            'dias_cons_even_cont' => 'Número de dias que el empleado trabajó eventualmente, en consejo y con contrato',
            'anios_otras_inst' => 'Número de años que el empleado trabajó en otras instituciones dependientes',
            'meses_otras_inst' => 'Número de meses que el empleado trabajó en otras instituciones dependientes',
            'dias_otras_inst' => 'Número de dias que el empleado trabajó en otras instituciones dependientes',
            'ubicacion' => 'URL que redirecciona a la ubicación del domicilio en google maps',
            'id_gradoestudio' => 'Clave Foranea de la tabla emp_gradoestudio, que indica el grado de formación académica del empleado',
            'titulo_obtenido' => 'Título obtenido en estudios realizados',
            'registra_asistencia' => 'Indica si el empleado registra o no asistencia',
            'reside_capital' => 'Indica si el empleado reside o no en capital',
            'fechareg_aniosservicio' => 'Fechareg Aniosservicio',
            'antecedentes' => 'Antecedentes',
            'progenitor_gestante' => 'Progenitor Gestante',
            'correo_institucional' => 'Correo Institucional',
            'usuario_dominio' => 'Usuario Dominio',
            'direccion_extranjero' => 'Direccion Extranjero',
            'cobro' => 'Cobro',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsisDuodecimas()
    {
        return $this->hasMany(AsisDuodecima::className(), ['id_empleado' => 'id_empleado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsisVacacions()
    {
        return $this->hasMany(AsisVacacion::className(), ['id_empleado' => 'id_empleado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsisVacacionempleados()
    {
        return $this->hasMany(AsisVacacionempleado::className(), ['id_empleado' => 'id_empleado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeclaracionJuradas()
    {
        return $this->hasMany(DeclaracionJurada::className(), ['id_empleado_declaracion' => 'id_empleado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpAniosservicios()
    {
        return $this->hasMany(EmpAniosservicio::className(), ['id_empleado' => 'id_empleado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpContratos()
    {
        return $this->hasMany(EmpContrato::className(), ['id_empleado' => 'id_empleado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpDeclaracionjuradas()
    {
        return $this->hasMany(EmpDeclaracionjurada::className(), ['id_empleado' => 'id_empleado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdGradoestudio()
    {
        return $this->hasOne(EmpGradoestudio::className(), ['id_gradoestudio' => 'id_gradoestudio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNacionalidad0()
    {
        return $this->hasOne(EmpPais::className(), ['nacionalidad' => 'nacionalidad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipodocumento()
    {
        return $this->hasOne(EmpTipodocumento::className(), ['id_tipodocumento' => 'id_tipodocumento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdGruposanguineo()
    {
        return $this->hasOne(EmpTiposangre::className(), ['id_tiposangre' => 'id_gruposanguineo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstadocivil()
    {
        return $this->hasOne(GralEstadocivil::className(), ['id_estadocivil' => 'id_estadocivil']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpKardexes()
    {
        return $this->hasMany(EmpKardex::className(), ['id_empleado' => 'id_empleado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpLlamadaatencions()
    {
        return $this->hasMany(EmpLlamadaatencion::className(), ['id_empleado' => 'id_empleado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpMemoEmpleados()
    {
        return $this->hasMany(EmpMemoEmpleado::className(), ['id_empleado' => 'id_empleado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpPrerrequisitoempleados()
    {
        return $this->hasMany(EmpPrerrequisitoempleado::className(), ['id_empleado' => 'id_empleado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpProcesos()
    {
        return $this->hasMany(EmpProceso::className(), ['id_empleado' => 'id_empleado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsrUsuarios()
    {
        return $this->hasMany(UsrUsuario::className(), ['id_empleado' => 'id_empleado']);
    }

    public function listarEmpleadosContrato($numdocumento)
    {

        $consulta = "SELECT
  ee.id_empleado,
  numdocumento,
  concat(nombre, ' ', paterno, ' ', materno) AS nombres,
  ec.nro_item,
  eca.descripcion                               cargo,
  ea.descripcion                                unidad,
  CASE WHEN ee.acobro = 1::bit
    THEN 'SI'
  ELSE 'NO' END                              AS cobrado
FROM emp_empleado ee, emp_contrato ec, emp_areatrabajo ea, emp_cargo eca
WHERE numdocumento = '" . $numdocumento . "' AND ee.activo = '1'
      AND ee.id_empleado = ec.id_empleado
      AND ec.actual = '1'
      AND ec.id_area = ea.id_area
      AND ec.id_cargo = eca.id_cargo";
        $empleadoContrato = Yii::$app->db->createCommand($consulta)->queryAll();;
        return $empleadoContrato;

        /*
                select  ee.id_empleado, numdocumento, nombre ||' ' || paterno ||' ' || materno nombres,nro_item, eca.descripcion cargo, ea.descripcion unidad, cobro
        from emp_empleado ee , emp_contrato ec, emp_areatrabajo ea, emp_cargo eca
        where numdocumento ='3755754' and ee.activo='1'
            and ee.id_empleado=ec.id_empleado
            and ec.actual='1'
            and ec.id_area=ea.id_area
            and ec.id_cargo=eca.id_cargo

        */
    }
}
